Clone repository

Download IGV and copy igv.jar into the sub-directory:
igv_plotter/IGV

Usage:
igv_plotter/IGV_plotter -LOCUS_SAMPLES igv_runs.txt -SAMPLES_TO_BAMS sample_bam.txt -REFERENCE hg19 -DEFAULT_EXTENT 100 -MAX_PANEL_HEIGHT_PIXELS 500 -SNAPSHOT_DIR . -IGV_PORT 60151 -JAVA_MEMORY 2g -COLLAPSE -NO-DISPLAY -IGV


A typical use case is:

igv_plotter/IGV_plotter \
-L example.locus_samples.txt \
-SAMPLES sample_bam.txt

where example.locus_samples.txt has:
a       1:152327502+50  1353-1  1353-2  1353-3
b       4:153549752     3002-1  3002-2  3002-3
c       5:82837197-82837397     1531-1  1531-2  1531-3

and the "SAMPLES" file maps samples to BAMs (in the 2nd column).
e.g.,

samp1	BAM_PATH_1
samp2	BAM_PATH_2

[Assuming one BAM per sample and one sample per BAM]


This then outputs a.png showing the reads for the 1353 "family" of samples at 1:152327502 +- 50 bp, b.png for locus 4:153549752 with the 3002 samples, and so forth...