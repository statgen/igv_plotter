#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use File::Basename;
use POSIX;

use IO::Socket::INET;
# flush after every write:
$| = 1;


my $WORKING_DIRECTORY = `pwd`;
chomp($WORKING_DIRECTORY);

my $COMMAND_DEREFERENCED = `readlink -f $0`;
die("Unable to determing actual directory of: $0") if ($?);
my ($COMMAND_FILE_NAME, $COMMAND_DIR) = fileparse($COMMAND_DEREFERENCED);

my $LOCUS_SAMPLES = "-";
my $SAMPLES_TO_BAMS = undef;
my $REFERENCE = "hg19";
my $SNAPSHOT_DIR = "$WORKING_DIRECTORY";

my $IGV_PORT = 60151;

my $JAVA_MEMORY = "2g";

my $DISPLAY = 0;
my $IGV = 1;

#my $DEFAULT_EXTENT = 20;
my $DEFAULT_EXTENT = 100;

my $MAX_PANEL_HEIGHT_PIXELS = 500;
#my $MAX_PANEL_HEIGHT_PIXELS = 250;

my $COLLAPSE = 1;

my $DEFAULT_USAGE = getUsageString(1);

GetOptions( "LOCUS_SAMPLES=s" => \$LOCUS_SAMPLES,
	    "SAMPLES_TO_BAMS=s" => \$SAMPLES_TO_BAMS,
	    "REFERENCE=s" => \$REFERENCE,
	    "SNAPSHOT_DIR=s" => \$SNAPSHOT_DIR,
	    "IGV_PORT=i" => \$IGV_PORT,
	    "JAVA_MEMORY=s" => \$JAVA_MEMORY,
	    "DISPLAY!" => \$DISPLAY,
	    "IGV!" => \$IGV,
	    "DEFAULT_EXTENT=i" => \$DEFAULT_EXTENT,
	    "MAX_PANEL_HEIGHT_PIXELS=i" => \$MAX_PANEL_HEIGHT_PIXELS,
	    "COLLAPSE!" => \$COLLAPSE
          ) || die($DEFAULT_USAGE);

if (!defined($SAMPLES_TO_BAMS)) {
  die($DEFAULT_USAGE);
}

print "Program was run as:\n".getUsageString();

my @CHILDREN_PIDS;
$SIG{'INT'} = \&CHILD_KILL_HANDLER;
$SIG{'TERM'} = \&CHILD_KILL_HANDLER;
$SIG{'QUIT'} = \&CHILD_KILL_HANDLER;
$SIG{'KILL'} = \&CHILD_KILL_HANDLER;
$SIG{'__DIE__'} = \&CHILD_KILL_HANDLER;

$SIG{'CHLD'} = \&TERM_CHILD_HANDLER;



my %SAMPLE_TO_BAM;
readSampleToBamList();

my $IGV_SOCKET = undef;
if ($IGV) {
  if (!exists($ENV{'HOME'})) {
    warn("IGV may fail since cannot find HOME directory to place default IGV port properties");
  }
  else {
    my $HOME = $ENV{'HOME'};
    my $IGV_PROPERTIES_SOURCE = "$COMMAND_DIR/IGV/prefs.properties";
    die ("Unable to locate file: $IGV_PROPERTIES_SOURCE") if (!-e $IGV_PROPERTIES_SOURCE);

    my $IGV_PROPERTIES_TARG = "$HOME/igv/prefs.properties";
    print `mkdir -p $HOME/igv && rm -f $IGV_PROPERTIES_TARG && cat $IGV_PROPERTIES_SOURCE | sed 's/REPLACE_PORT/$IGV_PORT/g' > $IGV_PROPERTIES_TARG`;
    die("Failed to set IGV port properties file") if ($?);
  }

  my $IGV_JAR = "$COMMAND_DIR/IGV/igv.jar";
  die ("Unable to locate file: $IGV_JAR") if (!-e $IGV_JAR);

  my $IGV_WRAPPER = "";
  if (!$DISPLAY) {
    #my $DISPLAY_DIMS = "1152x900x8";
    my $DISPLAY_DIMS = "3000x2000x24";
    print "Xvfb display= $DISPLAY_DIMS\n";

    my $XVFB_RUN_PATH = "$COMMAND_DIR/xvfb-run.sh";
    die ("Unable to locate file: $XVFB_RUN_PATH") if (!-e $XVFB_RUN_PATH);
    $IGV_WRAPPER = "$COMMAND_DIR/xvfb-run.sh -s '-screen 0 $DISPLAY_DIMS'";
  }
  # env HOME=$WORKING_DIRECTORY
  runProcess("$IGV_WRAPPER java -Xmx$JAVA_MEMORY -jar $IGV_JAR");

  my $SLEEP_SEC = 20;
  print "Sleeping for $SLEEP_SEC seconds to wait for IGV...\n";
  sleep($SLEEP_SEC);

  $IGV_SOCKET = new IO::Socket::INET (
				      PeerHost => '127.0.0.1',
				      PeerPort => $IGV_PORT,
				      Proto => 'tcp',
				     ) or die "ERROR in Socket Creation: $!\n";
}


runIGVcommand("genome $REFERENCE");
runIGVcommand("snapshotDirectory $SNAPSHOT_DIR");

open(STDIN, "<$LOCUS_SAMPLES") || die "can't open $LOCUS_SAMPLES: $!";

my $line = '';
while ($line = <STDIN>) {
  chomp($line);
  my @fields = split(/\s+/, $line);

  # Ignore comment lines (starting with '#'):
  next if (scalar(@fields) > 0 && substr($fields[0],0,1) eq '#');
  # Ignore empty lines:
  next if (scalar(@fields) == 0);

  die("Must have first two fields of: output name; locus to plot") if (scalar(@fields) < 2);

  my $outputBase = shift(@fields);
  my $locusExpression = shift(@fields);

  my ($locusCenter, $regionChr, $regionStart, $regionEnd, $sortMethod) = parseLocus($locusExpression);

  runIGVcommand("new");

  #runIGVcommand("region $regionChr $regionStart $regionEnd");
  #runIGVcommand("goto $locusCenter");
  my $gotoRegion = "$regionChr:$regionStart-$regionEnd";
  runIGVcommand("goto $gotoRegion");

  my @samples = @fields;
  foreach my $sample (@samples) {
    die ("No BAM file found for sample $sample") if (!exists($SAMPLE_TO_BAM{$sample}));
    my $bam = $SAMPLE_TO_BAM{$sample};

    runIGVcommand("load $bam");
    runIGVcommand("maxPanelHeight $MAX_PANEL_HEIGHT_PIXELS");
    runIGVcommand("collapse") if ($COLLAPSE);
  }

  runIGVcommand("goto $gotoRegion");
  runIGVcommand("sort $sortMethod");

  runIGVcommand("snapshot $outputBase.png");
}


runIGVcommand("exit") if (!$DISPLAY);

close(STDIN);

CHILD_KILL_HANDLER('QUIT') if (!$DISPLAY);






sub parseLocus {
  my ($locusExpression) = @_;
  my $origLocusExpr = $locusExpression;

  if ($locusExpression =~ m/[Cc][Hh][Rr](.+)/) {
    $locusExpression = "chr$1";
  }
  else {
    $locusExpression = "chr$locusExpression";
  }

  my ($locusCenter, $regionChr, $regionStart, $regionEnd, $sortMethod);

  if ($locusExpression =~ m/(chr.+):(.+)/) {
    $regionChr = $1;

    my $region = $2;

    if ($region =~ m/([0-9]+)(-|\.\.)([0-9]+)(\+[0-9]+){0,1}/) {
      $regionStart = $1;
      $regionEnd = $3;

      my $extent = 0;
      $extent = $4 if (defined $4);

      #print "regionStart= $regionStart\n";
      #print "regionEnd= $regionEnd\n";
      #print "extent= $extent\n";

      $regionStart = max(1, $regionStart - $extent);
      $regionEnd = $regionEnd + $extent;

      $locusCenter = int(($regionEnd + $regionStart) / 2);

      #$sortMethod = "position";
      #$sortMethod = "base $locusCenter";
      $sortMethod = "base";
    }
    elsif ($region =~ m/([0-9]+)(\+[0-9]+){0,1}/) {
      $locusCenter = $1;

      my $extent = $DEFAULT_EXTENT;
      $extent = $2 if (defined $2);

      $regionStart = max(1, $locusCenter - $extent);
      $regionEnd = $locusCenter + $extent;

      #$sortMethod = "base $locusCenter";
      $sortMethod = "base";
    }
    else {
      die("Invalid region: $region");
    }
  }
  else {
    die("Bad locus: $origLocusExpr");
  }

  $locusCenter = "$regionChr:$locusCenter";

  return ($locusCenter, $regionChr, $regionStart, $regionEnd, $sortMethod);
}



sub readSampleToBamList {
  open(SAMPLE_BAM, "<$SAMPLES_TO_BAMS") || die "can't open $SAMPLES_TO_BAMS: $!";

  my $line = '';
  while ($line = <SAMPLE_BAM>) {
    chomp($line);
    my @fields = split(/\s+/, $line);
    die("Invalid sample-to-bam mapping line: $line\n") if (scalar(@fields) < 2);

    $SAMPLE_TO_BAM{$fields[0]} = $fields[1];
  }

  close(SAMPLE_BAM);
}

sub runProcess {
  my ($command) = @_;

  my $pid = fork();
  die "unable to fork: $!" unless defined($pid);
  if (!$pid) {  # child
    print "Running: $command\n";
    exec($command);
    die "Unable to exec: $!";
  }

  push(@CHILDREN_PIDS, $pid);
  return $pid;
}

sub runIGVcommand {
  my ($command) = @_;

  if (!$IGV) {
    print "$command\n";
    return 1;
  }

  print $IGV_SOCKET "$command\n";
  return 1 if ($command eq 'exit');

  my $response = <$IGV_SOCKET>;
  print $response;

  return 1;
}

sub CHILD_KILL_HANDLER {
  $SIG{'CHLD'} = 'IGNORE';

  my $signal = 'KILL';
  $signal = $_[0] if (scalar(@_) > 0 && ($_[0] eq 'QUIT' || $_[0] eq 'INT'));

  for my $child (@CHILDREN_PIDS) {
    print "Killing child process $child with signal INT...\n" if ($signal eq 'KILL');
    kill('INT', $child);
    if (kill(0, $child) == 1) {
      print "Killing child process $child with signal $signal...\n" if ($signal eq 'KILL');
      kill($signal, $child);
      waitpid($child, 0);
    }
  }
}

sub TERM_CHILD_HANDLER {
  for my $child (@CHILDREN_PIDS) {
    die("Child processed terminated.\n") if (waitpid($child, &POSIX::WNOHANG));
  }
}

sub getUsageString {
  my $default = 0;
  ($default) = @_ if (scalar(@_) > 0);

  my $str = "";
  $str .= "Usage: " if ($default);
  $str .= "$0 -LOCUS_SAMPLES $LOCUS_SAMPLES -SAMPLES_TO_BAMS";

  $str .= " $SAMPLES_TO_BAMS" if (!$default);
  $str .= " [samples_to_bams_file]" if ($default);

  $str .= " -REFERENCE $REFERENCE -DEFAULT_EXTENT $DEFAULT_EXTENT -MAX_PANEL_HEIGHT_PIXELS $MAX_PANEL_HEIGHT_PIXELS"
    ." -SNAPSHOT_DIR $SNAPSHOT_DIR -IGV_PORT $IGV_PORT -JAVA_MEMORY $JAVA_MEMORY";

  $str .= " -";
  $str .= "NO-" if (!$COLLAPSE);
  $str .= "COLLAPSE";

  $str .= " -";
  $str .= "NO-" if (!$DISPLAY);
  $str .= "DISPLAY";

  $str .= " -";
  $str .= "NO-" if (!$IGV);
  $str .= "IGV";

  $str .= "\n";

  return $str;
}

sub min {
  my ($a, $b) = @_;

  if ($a < $b) {
    return $a;
  }

  return $b;
}

sub max {
  my ($a, $b) = @_;

  if ($a > $b) {
    return $a;
  }

  return $b;
}
